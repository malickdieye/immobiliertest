<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depenses extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'depenes_id', 'user', 'libelle', 'montant', 'commentaire', 'fichier'
    ];
    /**
     * Get the Bailleurs for the blog post.
     */
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
