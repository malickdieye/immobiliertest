<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Depenses;
use App\Equipements;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DepensesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return DB::table('depenses')
            ->where('user', $user->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            //'montant' => 'required|integer',
        ]);
        $montant = $request['montant'];
        $user = Auth::user();
        $user->solde =  $user->solde - $montant;
        $user->save();
        if ($request->fichier) {
            $fichier = $this->upload($request->fichier);
        } else {
            $fichier = null;
        }

        return Depenses::create([
            'libelle' => $request['libelle'],
            'montant' => $montant,
            'fichier' => $fichier,
            'commentaire' => $request['commentaire'],
            'user' => $user->id
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'libelle' => 'required|string|max:191',
            'montant' => 'required|integer'
        ]);
        $depense = DB::table('depenses')->where('depenses_id', $id);
        $user = Auth::user();
        if ($request['montant'] !== $depense->first()->montant) {
            $user->solde =  $user->solde + $depense->first()->montant;
            $user->solde =  $user->solde - $request['montant'];
        }

        $depense->update(['libelle' => $request['libelle'], 'montant' => $request['montant'], 'commentaire' => $request['commentaire']]);
        $user->save();
        return ['message' => 'Dépense modifié'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $depense = DB::table('depenses')->where('depenses_id', $id);

        $montant = $depense->first()->montant;
        $user = Auth::user();
        $user->solde =  $user->solde + $montant;
        $user->save();
        $depense->delete();
        return ['message' => 'dépensesupprimé avec succès'];
    }

    public function upload($var)
    {


        $file = $var;



        //Display File Name
        echo 'File Name: ' . $file->getClientOriginalName();
        echo '';
        //Display File Extension
        echo 'File Extension: ' . $file->getClientOriginalExtension();
        echo '';
        //Display File Real Path
        echo 'File Real Path: ' . $file->getRealPath();
        echo '
        ';
        //Display File Size
        echo 'File Size: ' . $file->getSize();
        echo '
        ';
        //Display File Mime Type
        echo 'File Mime Type: ' . $file->getMimeType();
        //Move Uploaded File
        $destinationPath = 'img/profile/';
        $file->move($destinationPath, $file->getClientOriginalName());
        return $file->getClientOriginalName();
    }
}
